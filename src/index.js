import React from 'react';
import { render } from 'react-dom';
import Root from './containers/Root';
// import Demo from './components/Demo';


render(
    <Root />,
    // <Demo />,
    document.getElementById('main')
);
