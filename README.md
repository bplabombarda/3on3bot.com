# 3on3bot.com [![forthebadge](http://forthebadge.com/images/badges/as-seen-on-tv.svg)](http://forthebadge.com)

### Website for [@3on3bot](http://twitter.com/3on3bot).

### What the heck do I do now?

-  `yarn install`
-  `npm start`: start development server.
-  `npm run build`: clean `dist` directory and build app.
-  `npm run watch`: watch with webpack.

![Rosie](src/assets/images/rosie.jpg)
